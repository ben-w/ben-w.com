---
title: Me
date: 2021-01-08T08:40:03Z
lastmod: 2021-01-08T08:40:03Z
---
### About me | [My CV](/files/Ben-Wilson-CV.pdf)
I am a C# programmer who has focused on desktop experiences for the past five years in my spare time. I mostly develop with Unity Game Engine, but also have significant experience in WPF for desktop apps, website development and cloud deployment.

I am keen to progress into a full-time developer role - excited to apply my skills and improve upon them.

| Skills | Tools         |
|--------|---------------|
| C#     | Unity         |
| Python | Visual Studio |
| HTML   | JetBrains IDE's           |
| CSS    | Git Command Line    |
