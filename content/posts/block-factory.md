---
title: Block Factory
date: 2018-05-11T00:00:00Z
lastmod: 2021-01-08T09:46:22Z
cover: /img/block-factory/board-2.png
categories:
  - College
tags:
  - Unity
  - Voxel
---
For my second year studying Games Development at Northbrook, I created Block Factory as my first 3D game. It was a factory building game similar to Factorio at the time.

<!--more-->

For this final major project, the theme was "Revolution", my game linked by the Industrial Revolution where factories started taking over farming and becoming more and more common. A part of the requirements, we had to document the initial research, detailed research, tests and presentation. This was all done on a free WordPress site.

I created all the asset inside of Magica Voxel, I hadn't known how voxel art was created, so when finding out about this software in my research was such a relief that I could create simple 3D models. Magica Voxel was also useful for rendering some of the UI elements as you could render it out as an image instead of exporting as a model.

![Boost Pipe](/img/block-factory/boost-pipe.gif)

*Boost Pipe*

![Filter Pipe](/img/block-factory/filter-pipes.gif)

*Filter Pipe*

The project management was done on Trello, this helped me make a plan of what I needed to do without forgetting or getting distracted.

![Tello Board](/img/block-factory/trelloboard.png)

*Tello Board*

Looking back at this project the one big issue with it is the lighting. It was meant to be set inside a factory however I had no experience with how to create good lighting so I just ended up making everything a bright white which made it harder to tell the depth. Another challenge with this project was getting the pipes working where items would travel along and get filtered correctly, the final implementation wasn't good or optimised but it worked most of the time. 
## Links
- [Gameplay Video](https://www.youtube.com/watch?v=dh5bsGHr6OQ)
- [Gamejolt Page](https://gamejolt.com/games/BlockFactory/340392)
- [Itch.io Page](https://marsh-mello.itch.io/block-factory)
- [Magica Voxel](https://ephtracy.github.io/)
- [Trello](https://trello.com/)
- [Project Files (zip)](/files/block-factory/block-factory-project-files.zip)
## Images
![Poster](/img/block-factory/board-3.png)

*Poster (Yes the spelling mistake wasn't noticed until exhibition day)*

![Controls Poster](/img/block-factory/controls.png)

*Controls Poster*

![3rd Poster](/img/block-factory/board-2.png)

*Controls Poster*

![Upgrades](/img/block-factory/upgrades.png)

*Upgrades*

![Pipe Example](/img/block-factory/PipeExample.png)

*Pipe Example*

![Options Menu](/img/block-factory/options-menu.png)

*Options Menu*