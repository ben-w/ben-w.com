---
title: Battleships
date: 2019-01-10T00:00:00Z
lastmod: 2021-01-08T09:41:30Z
cover: /img/battleships/image-1.png
categories:
  - College
tags:
  - Unity
  - Networking
---
When I was studying Games Development at Northbrook MET, for Unit 12 we had to create a game based on the theme Nautical in two weeks. For this unit, I tried to create a ".io" style of game where you would play as pirates shooting each other.

<!--more-->


I was using Unity as I was much more comfortable creating things quickly and easily with it than Unreal Engine (the only two game engines we had installed on the computers). For networking, I used a library called, Forge Networking Remastered by Bearded Man Studios. This simplified the process with easy to use wizards for syncing variables.



We also had to research and write up all the information about the game. [Here](/files/battleships/nautical-map.pdf "PDF document") is my first mind map into the theme "Nautical". We would later have to come up with four different ideas ([Mind Map](/files/battleships/4-different-game-ideas-mind-map.pdf)) with some research behind them. And then pick one of them for a final proposal to the tutors. [Wordpress Site of four different ideas](https://benlevel3year2.wordpress.com/initial-ideas/)

The hard part I had with this project was fitting in the time frame, all the research, writing and creating assets. Little was actually programming, so the final outcome isn't very polished at all.
## Links
- [Wordpress Page](https://benlevel3year2.wordpress.com/final-product-2/)
- [Github Repo](https://github.com/MarshMello0/BattleShips)
## Images
![Image of game](/img/battleships/image-1.png "Main Menu")
*Main Menu*

![Image of game](/img/battleships/image-2.png "Host View of all the players chasing each other")
*Host View of all the players chasing each other*