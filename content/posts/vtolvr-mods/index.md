---
title: VTOL VR Mods
date: 2019-04-28T00:00:00Z
lastmod: 2021-01-08T10:02:03Z
cover: /posts/vtolvr-mods/Mod Loader.png
categories:
  - Personal
tags:
  - Tool
  - Unity
  - Modding
  - VR
weight: -1
---

VTOL VR Mods is a tool that allows players to mod their game with ease. With a Python web framework, C# WPF desktop application and a class library for the code in game, users can easily play and create their mods.

<!--more-->


This started as a solo project but now has progressed into a team with two other programmers which I lead. We are an international group that uses Gitlab to host our code and Discord for communications. 

The website is built using the Django web framework in Python, to serve up dynamic web pages on the user's request and respond to RESTful API requests. It is automatically kept up to date with a Gitlab Runner on our VPS which pulls and updates the production and dev site for us.

{{< image src="website.png" alt="Picture of vtolvr-mods.com" text="Screenshot of vtolvr-mods.com" >}}

For the Windows software, we use .NET Core with WPF to provide a tool that can patch the game, download and update installed mods, upload and release players' mods and diagnose issues. This is also open source, which means over the few years we have had a few contributions from outside the main team.

{{< image src="Mod Loader.png" alt="Picture of the desktop software" text="Screenshot of the desktop software made using .NET Core" >}}

The community has grown bigger than I ever expected it to be, with over 4k downloads, 3500 discord members and over £400 in donations. Over these three years has been a great experience. 


## List of tools
### Unity
Used to create the asset bundle for the in game user interface.
### Visual Studio / Jetbrains Rider
Used to create the WPF application and the dll for the injector to inject into the game when it starts.
### Visual Studio Code / Pycharm Community Edition
Used as my editor to create and edit the Django website in Python
### Git
The source control for all the code. Mod Loader, WPF app and website are all hosted on Gitlab.

# Links
- [Website](https://vtolvr-mods.com)
- [Gitlab Group](https://gitlab.com/vtolvr-mods)

# Images
{{< image src="ingame-ui.png" alt="Picture of the in game user interface" text="Image of the in game user interface" >}}
{{< image src="skins.png" >}}
{{< image src="skins2.jpg" text="Some skins made by the community">}}

{{< image src="mod.gif" text="A community mod to record VTOL VR gameplay and place it into Tacview for replaying.">}}