---
title: "Grapple Game"
date: 2021-10-14T13:37:00+01:00
cover: /posts/grapple-game/cover.png
categories:
  - Personal
tags:
  - Unity
  - Game Jam
weight: 1
---

Into Games hosted their first coding challenge. The theme was "Swing into Action".
By myself, I created Grapple Game to submit for the challenge.

<!--more-->
{{< itch id="1233822" bg="222222" fg="eeeeee" lc="21ba45" bc="bebebe" >}}

# Camera Movement

The games camera movement was created using [Unity's Cinemachine](https://unity.com/unity/features/editor/art-and-design/cinemachine), this allowed me to quickly make a nice camera controller without having to do much coding.

The intro used a dolly track with camera and a simple script to transision through all the points.
{{< image src="intro camera track.png" text="The intro's camera track" >}}


And the third person camera used Cinemachine's free look camera with the same sized top, middle and bottom rigs. 
{{< image src="Player Camera.png" text="Cinemachine's free look camera rig around the player">}}

# Scene Loader

For the loading scene after pressing play, I was heavliy inspired by the [loading scene from Human Fall Flat](https://youtu.be/ekjzzLoV06s?t=37 "Youtube Video of the loading scene"). Where when loading a level the players ragdoll would keep falling forever in the clouds and the next scene would load below them so they fall onto it.

The way I achived this was my having a `SceneLoader` class that had all the players ridigbodies stored in a list, then it would load the loading scene, move the game object and unload the main menu scene.

```cs
public static void LoadScene(Scenes scene) => _instance.LoadSceneRoutine(scene);

private void LoadSceneRoutine(Scenes scene)
{
    _targetScene = scene;
    SceneManager.LoadSceneAsync((int)Scenes.Loading, LoadSceneMode.Additive);
}

private void OnSceneLoaded(Scene scene, LoadSceneMode mode)
{
    if (scene.buildIndex == (int)Scenes.Loading)
    {
        SceneManager.MoveGameObjectToScene(PlayerObject, scene);
        SceneManager.UnloadSceneAsync((int)_currentScene);
        _currentScene = Scenes.Loading;
        SceneManager.LoadSceneAsync((int)_targetScene, LoadSceneMode.Additive);
        return;
    }

    if (scene.buildIndex != (int)_targetScene)
        return;
    
    SceneManager.MoveGameObjectToScene(PlayerObject, scene);
    SceneManager.UnloadSceneAsync((int)_currentScene);
    _currentScene = _targetScene;
}
```

Incase loading took longer than I expected, I add a max falling speed into the loading scene. This prevented slower loading time giving the player move speed into the level from the gravity.

```cs
private void FixedUpdate()
{
    if (_currentScene != Scenes.Loading)
        return;
    
    Vector3 velocity = _playerRB.velocity;
    if (velocity.y < -_maxFallSpeed)
    {
        foreach (Rigidbody body in _rigidbodies)
        {
            Vector3 speed = body.velocity;
            speed.y = -_maxFallSpeed;
            body.velocity = speed;
        }
    }
}
```

# Links
- [Grapple Game on Itch.io](https://marsh-mello.itch.io/grapple-game)
- [Into Games](https://intogames.org)
- [Source Code on Gitlab](https://gitlab.com/ben-w/grapple-game)

# Images
{{< image src="Screenshot-4.png">}}
{{< image src="Screenshot.png">}}
{{< image src="Screenshot-2.png">}}
{{< image src="Screenshot-3.png">}}