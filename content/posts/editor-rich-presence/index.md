---
title: Editor Rich Presence
date: 2018-11-13T09:00:29Z
lastmod: 2021-01-08T09:48:22Z
cover: /posts/editor-rich-presence/SocialMediaImage.png
categories:
  - Personal
tags:
  - Unity
  - Tool
weight: -1
---

Editor Rich Presence is a free tool for Unity that displays what you are currently working on your Discord profile.

<!--more-->

I created this asset in 2018 and have been periodically updating it. It has a custom editor window that gives a preview.

{{<image src="example.png" text="Example of what is shows on your profile" >}}

## Tools
- C#
- Unity
- Visual Studio
- [Krita](https://krita.org/en/)

## Links
- [Unity Asset Store Page](https://assetstore.unity.com/packages/tools/utilities/editor-rich-presence-178736)
- [Source Code](https://github.com/MarshMello0/Editor-Rich-Presence)
## Images
{{< image src="EditorRichPresenceWindow.png" text="Custom editor window">}}

{{< image src="HiddenExample.png" text="Hidden Example" >}}