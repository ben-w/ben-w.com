---
title: Infected2d
date: 2017-08-03T09:51:44Z
lastmod: 2021-01-08T09:51:44Z
cover: /img/infected-2d/level3.png
categories:
  - Personal
tags:
  - Unity
---
Infected 2D was the first game I made in my spare time. It started as a project to learn how Unity worked.
<!--more-->
I was working alongside Mythological Head Football, this allowed me to play around with the engine in a relaxed environment, not having to document everything. For this first project, I didn't think of an original game idea, I saw a game called "Tag" on the Construct 2 Arcade (the other game engine I knew at the time) and I wanted to recreate it in Unity.

This first project was a great success at learning the engine while creating a basic prototype and towards the end, I started playing around with more features, like sending web request for updates, level creator and controller support.
## Links
- [Itch.io Page](https://marsh-mello.itch.io/infected-2d)
- [Game Jolt Page](https://gamejolt.com/games/Infected2d/302639)
- [Project Files (zip)](/files/infected-2d/Infected-2D.zip)
## Images
![First Level](/img/infected-2d/level.png)

First Level

![Second Level](/img/infected-2d/level2.png)

Second Level

![Third Level](/img/infected-2d/level3.png)

Third Level

![Level Creator](/img/infected-2d/levelcreator.png)

Level Creator

![Main Menu](/img/infected-2d/mainmenu.png)

Main Menu

![Settings](/img/infected-2d/settings.png)

Settings

![Title Screen](/img/infected-2d/titlescreen.png)

Title Screen