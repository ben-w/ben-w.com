---
title: Uconsole
date: 2019-08-08T09:59:42Z
lastmod: 2021-01-08T09:59:42Z
cover: /img/uconsole/uconsole.png
categories:
  - Personal
tags:
  - Unity
  - Tool
weight: 7
---
UConsole is a small standalone console you can add to existing games and then you can easily add custom commands. 
<!--more-->
I made this because lots of my projects, during development I wanted a console where I could spawn things or test actions without having to play the whole game to get to that part. It's meant to be a drag and drop so you don't have to do much work to it.

The main challenge with this project was that I hadn't done much work with actions/callbacks before. I would say that this project works, but it isn't perfect. It requires TextMeshPro because the default Unity text was blurry, and the code isn't optimised or thought through. 
## Links
- [Source Code (Github)](https://github.com/MarshMello0/UConsole)
## Images
![Ingame View](/img/uconsole/in-game-view.png)