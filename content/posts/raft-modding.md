---
title: Raft Modding
date: 2019-02-24T09:56:13Z
lastmod: 2021-01-08T09:56:13Z
cover: /img/raft-modding/RaftModding.png
categories:
  - Personal
tags:
  - Unity
  - Modding
weight: 9
---
I have created a few Raft Mods in the past and was heavily involved in helping out in the community from just running mods to people creating them.
<!--more-->
Mods were first created as class libraries then you would place the .dll in the mods folder but now you can just place CSharp files and it will compile it for you. All my mods were created by me alone and added small features to the game such as increasing a spawn rate of items, spawning items and increasing the size of nets.

As this was my first time modding a game built in the Unity engine there was a lot of exploring, finding new tools and finding different scripts/objects to do what you want to change. The hardest challenge of them all was trying to understand how Raft works in multiplayer. Syncing objects up and sending custom messages, but now after testing and experimenting, we have made it easier for future mod creators. 
## Links
- [My Raft Modding Profile](https://www.raftmodding.com/user/.%20Marsh.Mello%20.)
- [Basic Spawner Source Code (Github)](https://github.com/MarshMello0/BasicSpawner)
- [Raft Rich Presence Source Code (Github)](https://github.com/MarshMello0/RaftRichPresence)
- [Backup Mod Source Code (Github)](https://github.com/MarshMello0/BackUpMod)
## Images
![](/img/raft-modding/RaftModding.png)

![More Trash](/img/raft-modding/more-trash.png)

![Larger Nets](/img/raft-modding/larger-nets.png)