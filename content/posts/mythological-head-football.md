---
title: Mythological Head Football
date: 2017-05-25T09:53:54Z
lastmod: 2021-01-08T09:53:54Z
cover: /img/mythological-head-football/headfootball.png
categories:
  - College
tags:
  - Unity
  - Networking
---
Mythological Head Football was my final major project in Level 2 Games Development at Northbrook. The theme was Mythology and from inspired browser football games, I created a head football game where you would play as different mythological gods.
<!--more-->
This was around the time when I first started using Unity back in version 5.4 and learning C# at the same time. I am really happy with how this project turned out because I had high expectations when I created the plan for my WordPress site.

One of the biggest challenges about this project was the decision to try adding multiplayer using Photon Networking. Having done nothing like creating games before then trying to create a game with multiplayer in mind. It ended up working but it would constantly desync/lag, but glad I tried it and didn't give up. 

## Links
- [Built Game](/files/mythological-head-football/Mythological-Head-Football.zip)
- [College Wordpress Site](https://gamesdevelopmentblogweb.wordpress.com/)
## Images
![](/img/mythological-head-football/MainMenu.png)

Main Menu

![](/img/mythological-head-football/Online.png)

Online

![](/img/mythological-head-football/QuickMatch.png)

Quick Match

![](/img/mythological-head-football/SeasonMode.png)

Season Mode

![](/img/mythological-head-football/TournamentMode.png)

Tournament Mode