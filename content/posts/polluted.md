---
title: Polluted
date: 2019-05-23T09:54:53Z
lastmod: 2021-01-08T09:54:53Z
cover: /img/polluted/Polluted Board.png
categories:
  - College
tags:
  - Unity
---

Polluted was my final major project for my last year at college. We had 13 weeks to create a game based on a chosen word, this project the word was "Forgotten". I decided to create a survival game where you are placed in an abandoned city and you are left to survive. For this project we couldn't work in teams, so all the art and programming was done by me.
<!--more-->

### Procedural City Generation
The procedural city was split into two sections, the top being the industrial factories and the bottom being the housing for those workers. Then from there, I would spawn the assets I made onto the tile grid. Here is a top-down view with a line debugger I made to test the generation instead of spawning assets.

![Procedural City with Debug Lines](/img/polluted/changing-to-2-gaps.png)

Blue is a three lane road, like a motorway.
Green is a two lane road, like a "A" road.
White is just a normal street. One Lane.

### Inventory System

There were items that the player could collect to survive, these are the items the people left behind. So I had to create an inventory system to hold all of these items on the player. It had moving items around, splitting items and joining them back together. 

![Gif of inventory system](/img/polluted/inventory-at-the-end-of-week-7.gif)

Here is a gif of the inventory system working with a random image.

### Main Menu

To set the bar higher for these final major projects, I had a goal to release this on steam. So there would be a need for a main menu where people could change things to their liking. Also later down the line, I added in displaying the user's profile.

![Main Menu Gif](/img/polluted/the-main-menu-at-the-end-of-week-7.gif)
![Steamworks added](/img/polluted/steamworks-1.png)

This project didn't turn as well as I hoped, it had a procedural generating city with items spawning in the houses but it was very empty and not seeming very abandoned. The hardest part about this was completing the art as well as doing the programming, as we had to create the assets our selves it meant I wasted a lot of time trying to create models and textures. Some of it's best features were the procedural generating city and the inventory system. 
### Tutorial

A simple and quick tutorial was added in because I knew many people at the exhibition (parents) wouldn't know much about games, so this tutorial just explains the basic things like walking around, jumping and running. The text would change to green when the player has pressed that button.

![Tutorial](/img/polluted/tutorial-0.png)
![Tutorial](/img/polluted/tutorial-1.png)
![Tutorial](/img/polluted/tutorial-2.png)
## Links
- [Source Code (Github)](https://github.com/MarshMello0/Polluted)
- [Development Log on Wordpress](https://benlevel3year2.wordpress.com/project-management-dev-log-2/)
## Images
![Board 0](/img/polluted/board0.png)
![Board 1](/img/polluted/board1.png)
![Board 2](/img/polluted/board2.png)

The boards at the exhibition

![Old Road System](/img/polluted/roads-connecting-to-the-tier-before.png)

This was the old road system, where I didn't want straight lines everywhere like the USA, but later had to give up on that approach.

![Items spawning in buildings](/img/polluted/improvement-with-ui.png)
![Traffic Lights](/img/polluted/traffic-lights-placed-around.png)