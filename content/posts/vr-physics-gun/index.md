---
title: VR Physics Gun
date: 2021-03-29T00:00:00Z
lastmod: 2021-01-08T10:00:41Z
cover: /posts/vr-physics-gun/physicsgun.png
categories:
  - Personal
tags:
  - Unity
  - VR
weight: 6
---

A recreation of the Physics Gun from Garry's Mod in VR. The physics gun can pick up and move objects in the scene which have a rigidbody.

<!--more-->
This uses Steam VR Plugin and Unity 2020.3

{{< video src="VR Physics Gun" >}}

[*Click here for YouTube Video*](https://youtu.be/cKAwkK2ZaEI)

## How it moves objects
The physics gun makes use of rigidbodies and fixed joints to move objects around. The last ray target gets a fixed joint to the rigidbody object which has been picked up. Then when the last target moves, the held object follows.

{{< video src="End Movement" >}}

In this video, you can see the Fixed Joint getting created and destroyed in the inspector on the left-hand side.


## How the ray is created
The ray is created by splitting the current target distance into a set number of transforms. Using an array of floats in the inspector, I can set the amount of points along the ray and the delay in seconds which is used when tweaning them.

{{< video src="Ray Movement" >}}

In this video, each blue diamond is a target and you can see the delay between each target.

## How the gun is held
The gun uses an interface to be picked up and that gives the ability to pass through controller inputs to the class. This means you can easily add more intractable items to hold quickly.

![Interface](interface.png)

# Links
- [Youtube Video](https://youtu.be/cKAwkK2ZaEI)
- [Source Code (Gitlab)](https://gitlab.com/ben-w/vr-physicsgun)