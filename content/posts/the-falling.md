---
title: The Falling
date: 2019-06-13T09:57:37Z
lastmod: 2021-01-08T09:57:37Z
cover: /img/the-falling/The Falling.png
categories:
  - College
tags:
  - Unity
  - Game Jam
---
The Falling was created for a game jam which was hosted at our college. The theme was "5 seconds to live" and we teamed up with people from different classes. 
<!--more-->
I ended up being in a small team of two, I was the coder and the other person was the artist, we decided to go for a simple platformer where you had to quickly pick the correct whole to fall into to collect the coin. You would have a brief time at the start to see where the coin was then the wholes would get covered up and you had to guess which one had the coin at the bottom.

We managed to complete the game in 24 hours we had, this was mainly because we had picked a simple idea and kept it simple. There were some challenges though, the main one being trying to export it as a WebGL project. As I had been testing it as a standalone exe there were some hidden issues when I switched the build to WebGL and uploaded it first to itch.io., one of the issues didn't get fixed in time but I am happy with the final outcome. 
## Links
- [Itch.io Page](https://marsh-mello.itch.io/the-falling)
## Images
![Main Menu](/img/the-falling/main-menu.png)

Main Menu

![Quick Preview](/img/the-falling/quick-preview.png)

Quick preview you get before they are hidden

![Finding Hole](/img/the-falling/finding-whole.png)

Trying to figure out where the correct hole is

![Death Screen](/img/the-falling/death-screen.png)

The death screen