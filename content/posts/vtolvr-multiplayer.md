---
title: VTOLVR Multiplayer
date: 2019-04-28T00:00:00Z
lastmod: 2021-01-08T10:03:46Z
cover: /img/vtolvr-multiplayer/solo-test.png
categories:
  - Personal
tags:
  - Modding
  - Unity
  - Networking
  - VR
weight: 4
---
VTOL VR is a singleplayer flight game on steam. One of the big reasons I started creating a mod loader was to have a go at adding in multiplayer into it with hopefully it being the main multiplayer with mods as well. The solo developer of the game had stated that he didn't want to start working on multiplayer or mod support till the game had been released out of early access.

<!--more-->

Once the mod loader was barely functional I started the multiplayer mod. As this being a mod I had very limited tools and couldn't easily just press play from a scene inside the Unity editor. I had to launch the game, wait through the cut scene and get to the point I wanted to test. This made it a lot harder but I enjoyed the challenge.

I first started with a networking solution called [DarkRift 2](https://www.darkriftnetworking.com/DarkRift2). Users would host a server and then connect that with their clients. This meant there could be 24/7 servers with large amounts of players (the dream). As I first started to make progress on this, I came to realise as the server was just a standalone application, it knew nothing about the game like AI's paths for that scenario, objects etc. So I opted for a simpler approach which would be peer to peer networking. 
(All the earlier videos I created and linked below were at this stage of development)

A user would host the game then people would join them and the host's game handed the scenario, ai, objectives etc. I tried using [Forge Networking Remastered](https://github.com/BeardedManStudios/ForgeNetworkingRemastered) as I have got experience in using that before inside of Unity, however, it heavily requires to be set up inside the editor and I couldn't manage to get it modded in. So I ended up using Steam's built-in P2P system in SteamWorks.

I made a lot of progress using [SteamWorks](https://steamworks.github.io/). I got players vehicles synced, positions (fighting against a floating-point system), and weapon load-outs and shooting of bullets, not missiles. However, when the mod loader started growing in popularity, I started realising all the flaws in it and how hard it was for users to create and share mods. So at the end of February, I stopped working on it to focus on improving the Mod Loader experiense.

As there was so much demand for it, people kept "googling" vtolvr multiplayer and coming across my repo and didn't understand why they couldn't download it. I ended up making the repo private to stop the constant messages. Some users did happen to fork it before I set it private and in the past month, some people have [continued](https://github.com/Temperz87/VTOLVR-Multiplayer/graphs/contributors "The public repo where a few members have continued development") where I left off.

As of the 22nd of August they have released the multiplayer mod. I have now been helping them if they get stuck with something with Unity or C# in the public modding discord.

Here are some early video clips of the progress I made

- [VTOL VR MP Testing Fast speed (08 Jun 2019)](https://youtu.be/k9nQ2c60gcE)
- [VTOL VR MP: Networking other players (25 Jun 2019)](https://youtu.be/geokxwsuFlA)
- [Multiplayer flight test (26 Jun 2019)](https://youtu.be/q2QxovW4fpY)

![Gif of Ketkevs point of view when just testing positions](/multiplayer_test_2019-06-26.gif)

*Gif of Ketkev's point of view when just testing positions*


| Languages and Tools | Use |
| --------- | ----- |
| C#, Visual Studio | Ingame Mod Loader |
| dnSpy, UTinyRipper, Unity | Decompiling/Viewing the game |
| Git | Github Repo |


# Links
- [My Old Github Repository](https://github.com/MarshMello0/VTOLVR-Multiplayer)
- [Community Fork to complete it](https://github.com/Temperz87/VTOLVR-Multiplayer)
- [Completed Multiplayer Mod on vtolvr-mods.com](https://vtolvr-mods.com/mod/qs6jxkt2/)

# Images
![Shooting Synced Video](/img/vtolvr-multiplayer/shooting-synced.gif)

Shooting Synced

![The final image of the website](/img/vtolvr-multiplayer/finalimage.jpg)

The final image made for the mod page on vtolvr-mods.com