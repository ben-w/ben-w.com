---
title: Simple Bullet Penetration
date: 2021-01-07T03:44:17Z
lastmod: 2021-02-09T15:57:17Z
cover: /img/unity-bullet-penetration/cover.png
categories:
  - Personal
tags:
  - Unity
weight: 5
---
This project is a simple FPS prototype where I experiment in Unity how you could create bullet penetration with the built-in physics.
<!--more-->
When I started this project, I was trying to affect the velocity of the bullet as well depending on the thickness of the wall. However, as I later realised that this would be harder down the line when dealing with higher speeds.


![Gif of what it first started out as](https://gitlab.com/ben-w/unity-bullet-penetration/-/raw/master/Videos/oldgif.gif)

*My first attempt at this*

## How it works
The bullet is just a rigidbody with no gravity. When the player shoots, it gets sent at a consistent speed in the direction the player was aiming. 

Then a raycast is being sent each frame from that rigidbody's position. If there is only one wall in between those two, it sends a raycast back from its current position then uses that hit point and the first hit point to work out the thickness. If there are multiply walls, it will put them in order and then send raycasts back to the previous wall to work out the distance. Lastly, if the object is inside a wall during the frame. It will try to send a raycast forward to see if there is anything to bounce off of, if that fails, it sends a ray back from a far distance away.

For targets to receive damage. The Monobehavoir class just needs to derive from `IDamageable` which gives an interface for the bullet to send the amount of damage to the target.

![cover image](/img/unity-bullet-penetration/cover.png)

This project might not turn into much, but I know in the future I can refer back to this source code in case I do need to recreate something like this in a bigger project.


## Links
- [Source Code (Gitlab)](https://gitlab.com/ben-w/unity-bullet-penetration)
- [Video of me shooting through some walls](https://gitlab.com/ben-w/unity-bullet-penetration/-/raw/master/Videos/Video.mp4)
## Images

![Gif through targets](/img/unity-bullet-penetration/Gif.gif)

Shooting through tagetes dealing different amounts of damage.